
import {Nav, Navbar, Container, } from 'react-bootstrap';  
import {Link} from 'react-router-dom'



function Navigation() {  
  return (  
    <Navbar bg="black " expand="md">  
    <Container c >  
      <Navbar.Brand style={{color:'white'}}>AgriTech</Navbar.Brand>  
      <Navbar.Toggle aria-controls="basic-navbar-nav"  style={{backgroundColor:'white'}}/>  
      <Navbar.Collapse id="basic-navbar-nav">  
        <Nav className="m-auto">  
          <Nav.Link><Link to='/Home' style={{textDecoration:'none'}}>Home</Link></Nav.Link>  
          <Nav.Link><Link to='/AboutUs' style={{textDecoration:'none'}}>AboutUs</Link></Nav.Link>  
        
          <Nav.Link><Link to='/Login' style={{textDecoration:'none'}}>Login</Link></Nav.Link>  
          <Nav.Link><Link to='/Register' style={{textDecoration:'none'}}>Register</Link></Nav.Link>   
          {/* <NavDropdown title="Dropdown" id="basic-nav-dropdown" style={{textDecoration:'none',color: 'white' }}>  
            <NavDropdown.Item href="#action/3.1"  style={{textDecoration:'none',color: 'white' }}></NavDropdown.Item>  
            <NavDropdown.Item href="#action/3.2">Dropdown Item 2</NavDropdown.Item>  
            <NavDropdown.Item href="#action/3.3">Dropdown Item 3</NavDropdown.Item>  
            <NavDropdown.Divider />  
            <NavDropdown.Item href="#action/3.4">Another Item</NavDropdown.Item>  
          </NavDropdown>   */}
        </Nav>  
      </Navbar.Collapse>  
    </Container>  
  </Navbar>  
  );  
}  
export default Navigation