import React, { useState } from 'react';
import {Carousel, CarouselItem, CarouselControl, CarouselIndicators, CarouselCaption} from 'reactstrap';
import carousal4 from '../images/carousal4.webp'
import carousal5 from '../images/coraousal5.jpg'
import carousal3 from '../images/carousal3.jpg'

const items = [
  {
    src: carousal4,
    altText: 'Slide 1',
    caption: 'OnlineShopping',
    key: 1,
  },
  {
    src: carousal5,
    altText: 'Slide 2',
    caption: 'Fresh products',
    key: 2,
  },
  {
    src: carousal3,
    altText: 'Slide 3',
    caption: 'Directy from field',
    key: 3,
  },
];

function Example(args) {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  const slides = items.map((item) => {
    
    return (
      <CarouselItem 
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src} 
      >
        <img src={item.src} alt={item.altText}  style={{ width:'100%',height:'35rem'}}/>
        <CarouselCaption
          captionText={item.caption}
          captionHeader={item.caption}
        />
      </CarouselItem>
    );
  });
  const carousals={
    border:'5px solid black',
    margin:'1rem auto',
    
  
}


  return (
    <Carousel  style={carousals}
      activeIndex={activeIndex}
      next={next}
      previous={previous}
      {...args}
    >
      <CarouselIndicators
        items={items}
        activeIndex={activeIndex}
        onClickHandler={goToIndex}
      />
      {slides}
      <CarouselControl
        direction="prev"
        directionText="Previous"
        onClickHandler={previous}
      />
      <CarouselControl
        direction="next"
        directionText="Next"
        onClickHandler={next}
      />
    </Carousel>
  );
}

export default Example;