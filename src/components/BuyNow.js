import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Form,Row,Col,FormGroup,Label,Input } from 'reactstrap'
import { toast,ToastContainer } from 'react-toastify';

function BuyNow() {
  const navigate=useNavigate();
  const form={
    border:'1px solid black',
    maxWidth: '800px',
   padding:'20px',
   margin: 'auto',
   backgroundColor: 'white',
   marginTop: '50px',
   color:'white',
   borderRadius: '10px',
   backgroundImage: 'url("https://a.rgbimg.com/users/o/or/organza3/300/mtgVC1W.jpg")',
   backgroundSize: 'cover',
 
   
  }
  const handleSubmit=(e)=>{
    e.preventDefault();
    toast.success('Item added successfully!', {
      onClose: () => {
        navigate('/Home');
      },
    });
  }
  

  return (
    <div>
      <Form style={form} onSubmit={handleSubmit}>
  <Row>
    <Col md={6} >
      <FormGroup>
        <Label for="Name" style={{color:'white'}}>
       Name
        </Label>
        <Input
          id="exampleEmail"
          name="name"
          placeholder="Enyter your full name"
          type="name"
          required
        />
      </FormGroup>
    </Col>
   
  </Row>
  <FormGroup>
    <Label for="exampleAddress" style={{color:'white'}}>
      Address
    </Label>
    <Input
      id="exampleAddress"
      name="address"
      placeholder=" ex: 145/2 Main St"
      required
    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleAddress2" style={{color:'white'}}>
      Address 2
    </Label>
    <Input
      id="exampleAddress2"
      name="address2"
      placeholder="Apartment, studio, or floor"
      
    />
  </FormGroup>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleCity" style={{color:'white'}}>
          City
        </Label>
        <Input
          id="exampleCity"
          name="city"
          placeholder="ex: Hyderabad"
          required
        />
      </FormGroup>
    </Col>
    <Col md={4}>
      <FormGroup>
        <Label for="exampleState" style={{color:'white'}}>
          State
        </Label>
        <Input
          id="exampleState"
          name="state"
          placeholder="ex: Telangana"
          required
        />
      </FormGroup>
    </Col>
    <Col md={2}>
      <FormGroup>
        <Label for="exampleZip" style={{color:'white'}}>
          Pin Code
        </Label>
        <Input
          id="exampleZip"
          name="zip"
          required
        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup check>
    <Input
      id="exampleCheck"
      name="check"
      type="checkbox"
      required
    />
    <Label
      check
      for="exampleCheck"
    >
      I am not Robot
    </Label>
  </FormGroup>
 
  <button class="btn btn-success" type='submit'>Place Order</button>
  
</Form>
<ToastContainer/>
    </div>
    
  )
}

export default BuyNow
