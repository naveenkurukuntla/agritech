import React from 'react';
import { Form, Row, Col, FormGroup, Label, Input, Button } from 'reactstrap';
import { useState } from 'react';
import axios from 'axios';
import { useNavigate,Link } from 'react-router-dom';
import { toast,ToastContainer } from 'react-toastify';



function SeedsSale() {
  
 
  const containerStyle = {
    maxWidth: '800px',
    margin: 'auto',
    backgroundColor: '#add8e6', 
    padding: '20px',
    borderRadius: '10px',
    boxShadow: '0px 0px 10px 0px rgba(0, 0, 0, 0.2)',
  };
  
  const formStyle = {
    marginTop: '20px',
    backgroundColor: 'white',
    padding: '20px',
    borderRadius: '10px',
    boxShadow: '0px 0px 10px 0px rgba(0, 0, 0, 0.1)',
  };
  
  let [formdata, setformdata] = useState({
    imgsrc:'',
    Name:'',
    Price:'',
   
  });

  const handleOnChange = (e) => {
    let { name, value } = e.target;
    setformdata({
      ...formdata,
      [name]: value,
    });
  };
  let navigate = useNavigate();
  const handleSubmit = async (e) => {
    e.preventDefault();
  
   

    try {
      let response = await axios.post('http://localhost:8080/Seedsinsert', formdata);
  
    console.log(response);
    toast.success('Item added successfully!', {
      onClose: () => {
        
        navigate('/');
      },
    });
  } catch (err) {
       console.error(err);
     }
  };

 

  return (
    <div>
      <h1 style={{textAlign:'center'}}>Registration Form</h1>
      <div style={containerStyle}>
        <Form style={formStyle}>
        <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="exampleFirstName">Img Url</Label>
                <Input
                  id="exampleFirstName"
                  name="imgsrc"
                  placeholder="Enter First Name"
                  type="text"
                  value={formdata.imgsrc}
                  onChange={handleOnChange}
                />
              </FormGroup>
            </Col>
           
            
          </Row>

          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="exampleFirstName">Item Name</Label>
                <Input
                  id="exampleFirstName"
                  name="Name"
                  placeholder="Enter First Name"
                  type="text"
                  value={formdata.Name}
                  onChange={handleOnChange}
                />
              </FormGroup>
            </Col>
           
            
          </Row>
         
          <FormGroup>
            <Label for="exampleCountry">Price</Label>
            <Input
              id="exampleCountry"
              name="Price"
              placeholder="Enter Price"
              type="text"
              value={formdata.Price}
              onChange={handleOnChange}
            />
          </FormGroup>
          
          <Button color="primary" onClick={handleSubmit}>
          <Link to='/Seeds' style={{color:'white'}}>
           Sale Item
           </Link>
          </Button>
          
        

        </Form>
        <ToastContainer />
      </div>
     
    </div>
  );
}

export default SeedsSale