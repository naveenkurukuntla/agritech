import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Card, CardBody, CardTitle, CardSubtitle,Button} from 'reactstrap'
import styled from 'styled-components'
import {useNavigate}from 'react-router-dom'



function Vegitables() {

  const StyleCard=styled.div`
  transition:transform 0.3s;
  &:hover{
    transform: scale(1.05);
    box-shadow:0 0 10px rgba(0,0,0,0,2);
  }`
  ;
  const navigate=useNavigate();


  const CardStyles={
    display:'flex',
    flexWrap:'wrap',
    justifyContent:'center',
    alignContent:'center',
    alignItems:'center',
    marginLeft:'30px',
    marginRight:'80px',
  }
 
  let [data, setdata] = useState()
  useEffect(() => {
      const fetchData = async () => {
          try{
          let response = await axios.get("http://localhost:8080/vegitablesfetch")
          console.log(response.data);
          setdata(response.data)
          }
          catch(err){
               throw err;
          }
      }
      fetchData()
  }, [])
  let handleButtonClick=(e)=>{
    // e.preventDefault();
    navigate('/BuyNow')
  }
  return (
    <div style={CardStyles}>
      
      <div style ={{padding :'1.3rem', justifyContent:'center',borderTop:"2px solid gold"}}>
            <h3 style={{color:'black',margin:'1rem'}}><center><b>OUR <span style={{color:'gold'}}>Vegitables</span></b></center></h3>
            <p style={{color:'black',margin:'1.5rem',fontSize:'1rem'}}>Lorem Ipsum Dolor Sit Amet Consectetur Adipisicing Elit. Non, Dicta.Lorem Ipsum Dolor Sit Amet Consectetur Adipisicing Elit. Animi Nulla Sit Libero Nemo Fuga Sequi Nobis? Necessitatibus Aut Laborum, Nisi Quas Eaque Laudantium Consequuntur Iste Ex Aliquam Minus Vel? Ne</p>
            <div><a href='/VegitablesSale'><button style={{color:'white', backgroundColor:'black',padding:'5px',width:'10rem',borderRadius:'5px'}}>Sale Items</button></a></div>
        </div>
       
        {data &&data.map((item)=>(
        <Card
        style={{
          display:'flex',
          flexWrap:'wrap',
        
          border:'2px solid gold',
          backgroundColor:'black',
          margin:'1.2rem',
          justifyContent:'center'
        }}
      >
        <img style={{width:'10.5rem',height:'10rem' ,justifyContent:'Center',alignContent:'center', padding:'1px'}}
          alt="Sample"
          src={item.imgsrc}
        />
        
        <CardBody>
          <CardTitle tag="h5" style={{color:'yellow'}}>
         {item.Name}
          </CardTitle>
         
         
          <CardSubtitle
          style={{color:'white'}}
            tag="h6"
          >
          Price :{item.Price}
          </CardSubtitle>
         <StyleCard>
          <Button  onClick={()=>{handleButtonClick()}} style={{ marginTop:'1.5rem',backgroundColor:'gold'}}>
        Buy Now
      </Button>
      </StyleCard>

        </CardBody>
      </Card> 
      ))}

      
    </div>
  )
}

export default Vegitables