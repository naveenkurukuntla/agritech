import React from 'react'
import {Card,CardBody,CardTitle,CardSubtitle,CardText,Button} from 'react-bootstrap'
import foodgrain from '../images/foodgrain.jpg'
import seeds from '../images/seeds.jpeg'
import vegitables from '../images/vegitables.jpeg'
import dairy from '../images/dairy.jpeg'
import fruits from '../images/fruits.jpg'
import styled from 'styled-components';
import {  useNavigate } from 'react-router-dom';


function Cards() {
    const cards={
        display:'flex',
        flexWrap:'wrap',
        justifyContent:'space-around',
        backgroundColor:'gray',
        padding:'2rem',
      
    }
    const card={
      padding:'2rem',
     
    }


    const StyledCard = styled.div`
  transition: transform 0.3s;
  &:hover {
    transform: scale(1.05);
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
  }
`;
const navigate =useNavigate();

const handleButtonClick = () => {
 
  navigate('/FoodGrains');
};
const handleButtonClickseeds=()=>{
  navigate('/Seeds');
}
const handleButtonClick2=()=>{
  navigate('/Vegitables')
}
const handleButtonClick3=()=>{
  navigate('/Fruits')
}
const handleButtonClick5=()=>{
  navigate('/DairyProducts')
}




  return (
    <div style={cards}>
    <div style={card}>
    <StyledCard>
      <Card
  style={{
    width: '18rem',
    padding:'0.2rem',
    border:'4px solid black'
   
  }}
>
  <img
    alt="Sample"
    src={foodgrain}
    style={{height:'18rem',border:'2px solid black'}}
  /> 
  <CardBody>
    <CardTitle tag="h5">
      FoodGrains
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
      Card subtitle
    </CardSubtitle>
    <CardText>
    Cereals include rice, wheat, maize, jowar, barley, etc. These are a rich source of nutrients provided by the plants.
    </CardText>
    <StyledCard>
    <Button onClick={handleButtonClick}>
    To Buy it..
    </Button>
    </StyledCard>
  </CardBody>
</Card>
</StyledCard>
    </div>
    <div style={card}>
    <StyledCard>
      <Card
  style={{
    width: '18rem',
    padding:'0.2rem',
    border:'4px solid black'
  }}
>
  <img
    alt="Sample"
    src={seeds}
    style={{height:'18rem',border:'2px solid black'}}
  />
  <CardBody>
    <CardTitle tag="h5">
      seeds
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
      Card subtitle
    </CardSubtitle>
    <CardText>
    Direct seed application of seed treatments in a seed film coating is a safe and more sustainable seeds...
    </CardText>
    <StyledCard>
    <Button onClick={handleButtonClickseeds}>
    To Buy it..
    </Button>
    </StyledCard>
  </CardBody>
</Card>
</StyledCard>
    </div>
    <div style={card}>
    <StyledCard>
      <Card
  style={{
    width: '18rem',
    padding:'0.2rem',
    border:'4px solid black'
  }}
>
  <img
    alt="Sample"
    src={vegitables}
    style={{height:'18rem',border:'2px solid black'}}
  />
  <CardBody>
    <CardTitle tag="h5">
      vegetables
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
      Card subtitle
    </CardSubtitle>
    <CardText>
    Go4fresh is delivering fresh, healthy fruits and vegetables directly from the farm to consumer.
    </CardText>
    <StyledCard>
    <Button onClick={handleButtonClick2}>
    To Buy it..
    </Button>
    </StyledCard>
  </CardBody>
</Card>
</StyledCard>
    </div>
    <div style={card} >
    <StyledCard>
      <Card
  style={{
    width: '18rem',
    padding:'0.2rem',
    border:'4px solid black'
    
  }}
>
  <img
    alt="Sample"
    src={fruits}
    style={{height:'18rem',border:'2px solid black'}}
  />
  <CardBody>
    <CardTitle tag="h5">
      fruits
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
      Card subtitle
    </CardSubtitle>
    <CardText>
    As a small-scale fresh fruit and vegetable grower, you may consider selling directly to retailers.
    </CardText>
    <StyledCard>
    <Button onClick={handleButtonClick3}>
    To Buy it..
    </Button>
    </StyledCard>
  </CardBody>
</Card>
</StyledCard>
    </div>
    <div style={card}>
    <StyledCard>
      <Card
  style={{
    width: '18rem',
    padding:'0.2rem',
    border:'4px solid black'
  }}
>
  <img
    alt="Sample"
    src={dairy}
    style={{height:'18rem',border:'2px solid black'}}
  />
  <CardBody>
    <CardTitle tag="h5">
      Dairy Products
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
      Card subtitle
    </CardSubtitle>
    <CardText>
    We offer you so many different and heavenly dairy products. So start filling up your cart and place the order now....
    </CardText>
    <StyledCard>
    <Button onClick={handleButtonClick5}>
    To Buy it..
    </Button>
    </StyledCard>
  </CardBody>
</Card>
</StyledCard>
    </div>

    </div>
    
  )
}

export default Cards
