import React from 'react';
import { MDBFooter, MDBContainer, MDBRow, MDBCol, MDBIcon } from 'mdb-react-ui-kit';



export default function App() {
  return (
    <MDBFooter bgColor='light' className='text-center text-lg-start text-muted'>
      <section className='d-flex justify-content-center justify-content-lg-between p-4 border-bottom'>
        <div className='me-5 d-none d-lg-block'>
          <span>Get connected with us on social networks:</span>
        </div>

        <div>
          <a href='https://www.facebook.com/campaign/landing.php?campaign_id=14884913640&extra_1=s%7Cc%7C589460569891%7Cb%7Csign%20in%20to%20facebook%7C&placement=&creative=589460569891&keyword=sign%20in%20to%20facebook&partner_id=googlesem&extra_2=campaignid%3D14884913640%26adgroupid%3D128696221832%26matchtype%3Db%26network%3Dg%26source%3Dnotmobile%26search_or_content%3Ds%26device%3Dc%26devicemodel%3D%26adposition%3D%26target%3D%26targetid%3Dkwd-11079269337%26loc_physical_ms%3D9062142%26loc_interest_ms%3D%26feeditemid%3D%26param1%3D%26param2%3D&gad_source=1&gclid=CjwKCAiA8sauBhB3EiwAruTRJhbiNzHe9o9wBJZqa6k3CrZpXA3GNj69aMJoZSaL6D9xRExX_XSt2BoCKDUQAvD_BwE' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='facebook' />
          </a>
          <a href='https://twitter.com/i/flow/login' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='twitter' />
          </a>
          <a href='https://www.google.com/search?q=google+login+page&rlz=1C1RXMK_enIN975IN975&oq=google+login&gs_lcrp=EgZjaHJvbWUqCggBEAAYsQMYgAQyEQgAEEUYFBg5GIcCGLEDGIAEMgoIARAAGLEDGIAEMgcIAhAAGIAEMgwIAxAAGBQYhwIYgAQyBwgEEAAYgAQyBwgFEAAYgAQyBwgGEAAYgAQyBwgHEAAYgAQyBwgIEAAYgAQyBwgJEAAYgATSAQg2MDI5ajBqN6gCALACAA&sourceid=chrome&ie=UTF-8' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='google' />
          </a>
          <a href='https://www.instagram.com/' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='instagram' />
          </a>
          <a href='https://www.linkedin.com/' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='linkedin-in' />
          </a>
          <a href='https://github.com/zo0r/react-native-push-notification/issues/1324' className='me-4 text-reset'>
            <MDBIcon color='secondary' fab icon='github' />
          </a>
          
        </div>
      </section>

      <section className=''>
        <MDBContainer className='text-center text-md-start mt-5'>
          <MDBRow className='mt-3'>
            <MDBCol md='3' lg='4' xl='3' className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>
                <MDBIcon color='secondary' icon='gem' className='me-3' />
               Agri TEch
              </h6>
              <p>
                Hey! Here You can buy Any Products which is directy from the fields and Directly From Farmers...
              </p>
            </MDBCol>

            <MDBCol md='2' lg='2' xl='2' className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Products</h6>
              <p>
                <a href='/FoodGrains' className='text-reset'>
                  Food Grains
                </a>
              </p>
              <p>
                <a href='/Vegitables' className='text-reset'>
                 Vegitables
                </a>
              </p>
              <p>
                <a href='Fruits' className='text-reset'>
                  Fruits
                </a>
              </p>
              <p>
                <a href='DairyProducts' className='text-reset'>
                  Dairy Products
                </a>
              </p>
            </MDBCol>

            <MDBCol md='3' lg='2' xl='2' className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Useful links</h6>
              <p>
                <a href='#!' className='text-reset'>
                  Pricing
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Settings
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Orders
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Help
                </a>
              </p>
            </MDBCol>

            <MDBCol md='4' lg='3' xl='3' className='mx-auto mb-md-0 mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Contact</h6>
              <p>
                <MDBIcon color='secondary' icon='home' className='me-2' />
                Hyderabad,Telangana, India
              </p>
              <p>
                <MDBIcon color='secondary' icon='envelope' className='me-3' />
               naveen@example.com
              </p>
              <p>
                <MDBIcon color='secondary' icon='phone' className='me-3' /> + 9876543210
              </p>
              <p>
                <MDBIcon color='secondary' icon='print' className='me-3' /> + 9123456789
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </section>

      <div className='text-center p-4' style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
        © 2024 Copyright:
        <a className='text-reset fw-bold' href='/#'>
          N@veen.com
        </a>
      </div>
    </MDBFooter>
  );
}