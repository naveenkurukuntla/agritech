import './App.css';
import {BrowserRouter,Routes,Route} from 'react-router-dom'

import Home from './pages/Home';
import AboutUs from './pages/AboutUs';

import Login from './pages/Login';
import Register from './pages/Register';
import FoodGrains from './components/FoodGrains';
import BuyNow from './components/BuyNow';
import FoodGrainsSale from './components/FoodGrainsSale';
import Seeds from './components/Seeds';
import SeedsSale from './components/SeedsSale';
import Vegitables from './components/Vegitables';
import VegitablesSale from './components/VegitablesSale';
import FruitsSale from './components/FruitsSale';
import Fruits from './components/Fruits'
import DairyProducts from './components/DairyProducts';
import DairyProductsSale from './components/DairyProductsSale';




function App() {
  return (
    <div>
    <BrowserRouter>
   
    <Routes>
      <Route path='/' element={<Login/>}/>
      <Route path='/Home' element={<Home/>}/>
      <Route path='/AboutUs' element={<AboutUs/>}/>
     
      <Route path='/Login' element={<Login/>}/>
      <Route path='/Register' element={<Register/>}/>
      <Route path='/FoodGrains' element={<FoodGrains/>}/>
      <Route path='/BuyNow' element={<BuyNow/>}/>
      <Route path='/FoodGrainsSale' element={<FoodGrainsSale/>}/>
      <Route path='/Seeds' element={<Seeds/>}/>
      <Route path='/SeedsSale' element={<SeedsSale/>}/>
      <Route path='/Vegitables' element={<Vegitables/>}/>
      <Route path='/VegitablesSale' element={<VegitablesSale/>}/>
      <Route path='/Fruits' element={<Fruits/>}/>
      <Route path='/FruitsSale' element={<FruitsSale/>}/>
      <Route path='/DairyProducts' element={<DairyProducts/>}/>
      <Route path='/DairyProductsSale' element={<DairyProductsSale/>}/>
      <Route path='/BuyNow' element={<BuyNow/>}/>
      
    </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
