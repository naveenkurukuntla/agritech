import React from 'react';
import {
  MDBInput,
  MDBCol,
  MDBRow,
  MDBCheckbox,
  MDBBtn,
  MDBIcon
} from 'mdb-react-ui-kit';
// import axios from 'axios';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import  { toast,ToastContainer } from 'react-toastify';
// import login from '../images/login.jpg'




export default function App() {
  const loginform={
            border:' solid black',
          
          
            padding:'30px', 
        }
    
        //mediaquery
    
        if(window.innerWidth>700){
            loginform.width='40%';
            loginform.margin='8rem auto'
            loginform.height='400px'
        }
        if (window.innerWidth <= 700) {
            loginform.width = '80%'; 
            loginform.margin = '2rem auto'; 
            loginform.height='420px'
          }

          let navigate=useNavigate()
      let [logindata,setLoginData]=useState({
        email:'',
        password:''
      })
      const handleOnChange=(e)=>{
        let {name,value}=e.target

        setLoginData({
            ...logindata,
            [name]:value
        })
      }
      const handlesubmit=(e)=>{
        e.preventDefault();
        toast.success('Login successfully!', {
          onClose: () => {
            navigate('/Home');
          },
        });
      //  let login=async()=>{
      //   try{
      //       let response=await axios.post("http://localhost:8080/Login",logindata)
      //       if(response.data.ok===1){
      //         alert("Login sucessfull")
      //         setLoginData({
      //           email:'',
      //           password:'',
               
            
      //         })
      //       }
      //       navigate('/Home')
      //       }
      //   catch(err){
      //       console.log(err)
      //   }
        
    //    }
    // login()
      
      }
    
  return (
    <div>
    <form style={loginform} onSubmit={handlesubmit}>
       
      <MDBInput   className='mb-4' type='email' id='form2Example1' name="email" label='Email address'required value={logindata.email}
       onChange={handleOnChange} />
      <MDBInput className='mb-4' type='password' id='form2Example2' name="password"  label='Password' required value={logindata.password}
       onChange={handleOnChange} />

      <MDBRow className='mb-4'>
        <MDBCol className='d-flex justify-content-center'>
          <MDBCheckbox id='form2Example3' label='Remember me' defaultChecked />
        </MDBCol>
        <MDBCol>
          <a href='#!'>Forgot password?</a>
        </MDBCol>
      </MDBRow>

      <MDBBtn type='submit' className='mb-4' block >
        Sign in
      </MDBBtn>

      <div className='text-center'>
        <p>
          Not a member? <a href='Register'>Register</a>
        </p>
        <p>or sign up with:</p>
        

        <MDBBtn floating color="secondary" className='mx-1'>
          <MDBIcon fab icon='facebook-f' />
        </MDBBtn>

        <MDBBtn floating color="secondary" className='mx-1'>
          <MDBIcon fab icon='google' />
        </MDBBtn>

        <MDBBtn floating color="secondary" className='mx-1'>
          <MDBIcon fab icon='twitter' />
        </MDBBtn>

        <MDBBtn floating color="secondary" className='mx-1'>
          <MDBIcon fab icon='github' />
        </MDBBtn>
        <ToastContainer/>
      </div>
      
    </form>
    </div>
    
  );
}