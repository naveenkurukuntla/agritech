import React from 'react'
import Carousal from '../components/Carousal'
import Footer from '../components/Footer'
import Cards from '../components/Cards'
import Navigation from '../components/Navigation'


function Home() {
  return (
    <div>
      <Navigation/>
      <Carousal/>
      <Cards/>
<Footer/>
     
    </div>
  )
}

export default Home
