import React from 'react';
import {
  MDBBtn,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBInput,
  MDBIcon,
  MDBCheckbox
}
from 'mdb-react-ui-kit';
import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Form } from 'reactstrap';


function App() {
  let navigate = useNavigate();
  let [formdata, setformdata] = useState({
    Name: '',
    Email: '',
   Password: '',
  
   
  });

  const handleOnChange = (e) => {
    let { name, value } = e.target;
    setformdata({
      ...formdata,
      [name]: value,
    });
  };
  // const handleOnChangePassword = () => {
  //   try {
  //     if (formdata.Password === formdata.Repeatpassword) {
  //       throw new Error("Passwords matched");
  //     }else{
  //       alert("Password does not match")
  //     }
  //   } catch (err) {
  //     console.error(err);
  //     alert("Passwords do not match");
  //   }
  // };
 


  const handleSubmit = async (e) => {
    e.preventDefault();
  
    try {
      
      let response = await axios.post('http://localhost:8080/Insert', formdata);
      console.log(response);
      alert('Registered Successfully');
      navigate('/Login')
      setformdata({
        Name: '',
        Email: '',
       Password: '',
       Repeatpassword: '',
       
      });
    
    } catch (err) {
      console.error(err);
    }
  };
 

  return (
    <MDBContainer fluid >
<Form onSubmit={handleSubmit}>
      <MDBCard className='text-black m-5' style={{borderRadius: '25px'}}>
        <MDBCardBody >
          <MDBRow>
            <MDBCol md='10' lg='6' className='order-2 order-lg-1 d-flex flex-column align-items-center'>

              <p classNAme="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Sign up</p>

              <div className="d-flex flex-row align-items-center mb-4 ">
                <MDBIcon fas icon="user me-3" size='lg'/>
                <MDBInput label='Your Name' id='form1' type='text' className='w-100' name='Name'  required   value={formdata.Name}
                  onChange={handleOnChange}
/>
              </div>

              <div className="d-flex flex-row align-items-center mb-4">
                <MDBIcon fas icon="envelope me-3" size='lg'/>
                <MDBInput label='Your Email' id='form2' type='email'  name='Email'  required value={formdata.Email}
                  onChange={handleOnChange} 
/>
              </div>

              <div className="d-flex flex-row align-items-center mb-4">
                <MDBIcon fas icon="lock me-3" size='lg'/>
                <MDBInput label='Password' id='form3' type='password'  name='Password'  required  value={formdata.Password}       onChange={handleOnChange}
               pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@$!%*#?&]{8,}$"
              title="Password must be at least 8 characters long and contain at least one letter and one number."
            

                 
/>
              </div>

              <div className="d-flex flex-row align-items-center mb-4">
                <MDBIcon fas icon="key me-3" size='lg'/>
                <MDBInput label='Repeat your password' id='form4' type='password' name='Repeatpassword'  required  value={formdata.Repeatpassword}
                       onChange={handleOnChange}
                       pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@$!%*#?&]{8,}$"
                       title="Password must be at least 8 characters long and contain at least one letter and one number."
                      
         
/>
              </div>

              <div className='mb-4'>
                <MDBCheckbox name='flexCheck' value='' id='flexCheckDefault' label='Subscribe to our newsletter'  />
              </div>

              <MDBBtn className='mb-4' size='lg' type='submit' >Register</MDBBtn>

            </MDBCol>

            <MDBCol md='10' lg='6' className='order-1 order-lg-2 d-flex align-items-center'>
              <MDBCardImage src='https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp' fluid/>
            </MDBCol>

          </MDBRow>
        </MDBCardBody>
      </MDBCard>
      </Form>
    </MDBContainer>
  );
}

export default App;